import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatbotBoxComponent } from './Components/chatbot-box/chatbot-box.component';
import { SafeHtmlPipe } from './Pipe/safe-html.pipe';
import { UserAuthComponent } from './Components/UI/user-auth/user-auth.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatbotBoxComponent,
    SafeHtmlPipe,
    UserAuthComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
