import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  botOpened: boolean = false;
  userAuth: boolean = false;
  fullscreen: boolean = true;
  title: any = 'test';

  openBot() {
    this.botOpened = true;
  }

  closeBot() {
    this.botOpened = false;
    this.userAuth = false;
  }

  userAuthentication(e: boolean) {
    this.userAuth = e;
  }

}
