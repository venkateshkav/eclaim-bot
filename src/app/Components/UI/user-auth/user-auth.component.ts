import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html'
})
export class UserAuthComponent {

  @Output() authentication = new EventEmitter();
  password: string = 'bot@123';

  enteredPassword: string;

  authFlag: boolean = true;
  spinnerAni: boolean = false;

  removeAuthFlag() {
    this.authFlag = true;
  }

  authSubmitHandler() {
    if (this.enteredPassword.toLowerCase() === this.password) {
      this.spinnerAni = true
      setTimeout(() => {
        this.spinnerAni = false;
        this.authFlag = true;
        this.authentication.emit(true);
      }, 1500)
    } else {
      this.authFlag = false;
    }


  }

}
