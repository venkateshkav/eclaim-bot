import {Component, ElementRef, NgZone, OnInit, ViewChild,} from '@angular/core';
import {Message} from '../../Service/chat.service';
import {Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const annyang: any;

@Component({
  selector: 'app-chatbot-box',
  templateUrl: './chatbot-box.component.html',
  styleUrls: ['./chatbot-box.component.scss'],
  host: {
    '(document:keypress)': 'handleKeyboardEvent($event)'
  }
})


export class ChatbotBoxComponent implements OnInit {

  messages: Message[] = [];
  userEnterdText: string;
  botCallFlag = false;
  botStatusFlag: string;
  elem: any;
  currentQuestion = 'welcome';
  voiceMuted = false;
  synth = window.speechSynthesis;
  voiceVolume = 1;
  voiceActiveSectionDisabled: boolean = true;
  voiceActiveSectionError: boolean = false;
  voiceActiveSectionSuccess: boolean = false;
  voiceActiveSectionListening: boolean = false;
  voiceText: any;
  registerForm: FormGroup;

  lastClick = 0;

  isFormValid: boolean = false;

  botName = 'Moti';

  audio: any = new Audio();

  conversation = new Subject<Message[]>();
  messageMap = [
    // state - 1
    {
      question: ['welcome'],
      command: ['Welcome'],
      content:
        '<p>Welcome to RedCarpet, We are happy to help you.</p>' +
        '<h4>Please choose your preferred language</h4><br>' +
        '<a class="btn">1. English</a><br>' +
        '<a class="btn">2. हिन्दी</a><br>',
      prevoice: '',
      voice: [
        'Hello, Welcome to RedCarpet. Please choose your preferred language',
        'Press 1 for English',
        'हिंदी के लिए 2 दबाएं'
      ],
      voice_digits: {
        num_digits: 1,
        mapping: {
          '1': 'English',
          '2': 'हिन्दी'
        }
      }
    },

    // Stage 2
    {
      question: ['Please choose your preferred language'],
      command: ['English', '1. English', '1'],
      content:
        '<h4>We are happy to help you.</h4><br>' +
        '<a class="btn">1. Existing User</a><br>' +
        '<a class="btn">2. New User</a>',
      voiceFile: 'English-2',
      prevoice: [],
      voice: [
        'Press 1 for If you are an Existing User',
        'Press 2 for If you are a New User'
      ],
      voice_digits: {
        mapping: {
          '1': 'Existing User',
          '2': 'New User'
        }
      }
    },

    // state - 2

    {
      question: ['We are happy to help you.'],
      command: ['New User', '2. New User', '2'],
      content:
        '<div class="form-container">' +
        '<div class="user-form">' +
        '<h4>Please type your registered mobile number followed by #key</h4><br>' +
        '<form rel="Submit" id="form-1"  name="form" #f="ngForm">' +
        `<label>Please enter your registered mobile number</label><input type="text" id="mobile_no" required oninvalid="this.setCustomValidity('Please fill the Mobile Number')" (keyup.enter)="enterHashKey(box.value)" oninput="setCustomValidity('')"><br>` +
        // `<button type="submit" class="submit-btn">Submit</a></form>` +
        '</div> ' +
        '</div> ',
      voiceFile: 'English-3',
      prevoice: [],
      voice: [
        'Please type your registered mobile number followed by #key'
      ],
    },

    {
      question: ['Please type your registered mobile number followed by #key'],
      command: ['#'],
      content:
        '<h4>Please choose your options.</h4>' +
        '<a class="btn">1. How to use RedCarpet services?</a><br>' +
        '<a class="btn">2. Unable to upload documents</a><br>' +
        '<a class="btn">3. Why is my Profile on Waitlist?</a><br>' +
        '<a class="btn">4. Credit Limit not yet approved.</a><br>' +
        '<a class="btn">5. How to apply for a Card</a><br>' +
        '<a class="btn">6. What is the status of my Card Delivery?</a><br>' +
        '<a class="btn">7. How will I pay my dues?</a><br>' +
        '<a class="btn">8. How to Reload?</a><br>' +
        '<a class="btn">9. Why did my Profile get rejected?</a><br>' +
        '<a class="btn">#. the Main Menu</a><br>' +
        '<a class="btn">*. To repeat the Menu</a><br>' +
        '',
      voiceFile: '',
      prevoice: [
        'Press 1 for How to use RedCarpet services?',
        'Press 2 for Unable to upload documents',
        'Press 3 for Why is my Profile on Waitlist?',
        'Press 4 for Credit Limit not yet approved.',
        'Press 5 for How to apply for a Card',
        'Press 6 for What is the status of my Card Delivery?',
        'Press 7 for How will I pay my dues?',
        'Press 8 for How to Reload?',
        'Press 9 for Why did my Profile get rejected?',
        'Press # for the Main Menu',
        'Press * To repeat the Menu'
      ],
      voice: [],
      voice_digits: {
        mapping: {
          '1': 'How to use RedCarpet Services?',
          '2': 'Unable to upload documents',
          '3': 'Why is my Profile on Waitlist',
          '4': 'Credit Limit not yet approved',
          '5': 'How to apply for a Card',
          '6': 'What is the status of my Card Delivery?',
          '7': 'How will I pay my dues?',
          '8': 'How to Reload?',
          '9': 'Why did my Profile get rejected?',
          '#': 'Main Menu',
          '*': 'To repeat the Menu'
        }
      }
    },

    {
      question: ['Please choose your options.'],
      command: ['1', '1. How to use RedCarpet Services?', 'How to use RedCarpet Services?'],
      content:
        '<h4>How to use RedCarpet services?</h4><br>' +
        '<p>Dear User, Thank you for using our Inbound Services. As per your request please follow the below mentioned link to know more about our RedCarpet Services.</p>' +
        '<a href="https://www.redcarpetup.com/faq" target="_blank">https://www.redcarpetup.com/faq</a>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, Thank you for submitting your request soon you will get the requested information through an SMS.'
      ],
    },
    {
      question: ['Please choose your options.'],
      command: ['2', '2. Unable to upload documents', 'Unable to upload documents'],
      content:
        '<h4>Unable to upload documents</h4><br>' +
        '<p>Dear User, We would like to request you kindly reinstall or download the latest version of your RedCarpet app to get this issue sorted.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We would like to request you kindly reinstall or download the latest version of your RedCarpet app to get this issue sorted.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['3', '3. Why is my Profile on Waitlist?', 'Why is my Profile on Waitlist?'],
      content:
        '<h4>Why is my Profile on Waitlist?</h4><br>' +
        '<p>Dear User, In case your profile is in waitlisted for less than 3 months. We would request you to wait for sometime as your profile is under review. Soon you will be notified.</p>' +
        '<p>In case your profile is in waitlisted for more than 3 months, We regret to inform you that your profile has been rejected because it does not match with our defined parameters.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, In case your profile is in waitlisted for less than 3 months. We would request you to wait for sometime as your profile is under review. Soon you will be notified.',
        'In case your profile is in waitlisted for more than 3 months, We regret to inform you that your profile has been rejected because it does not match with our defined parameters.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['4', '4. Credit Limit not yet approved', 'Credit Limit not yet approved'],
      content:
        '<h4>Credit Limit not yet approved</h4><br>' +
        '<p>Dear User, For your information we could find a limit in your account of INR ____, Would request you kindly look into your app.</p>' +
        '<p>In case your profile is in waitlisted for more than 3 months, We regret to inform you that your profile has been rejected because it does not match with our defined parameters.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, For your information we could find a limit in your account of INR ____, Would request you kindly look into your app.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['5', '5. How to apply for a Card?', 'How to apply for a Card?'],
      content:
        '<h4>How to apply for a Card?</h4><br>' +
        '<p>Dear User, In case your profile is in waitlisted for less than 3 months. We would request you to wait for sometime as your profile is under review. Soon you will be notified.</p>' +
        '<p>In case your profile is in waitlisted for more than 3 months, We regret to inform you that your profile has been rejected because it does not match with our defined parameters.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, In case your profile is in waitlisted for less than 3 months. We would request you to wait for sometime as your profile is under review. Soon you will be notified.',
        'In case your profile is in waitlisted for more than 3 months, We regret to inform you that your profile has been rejected because it does not match with our defined parameters.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['6', '6. What is the status of my Card Delivery?', 'What is the status of my Card Delivery?'],
      content:
        '<h4>What is the status of my Card Delivery?</h4><br>' +
        '<p>Dear User, Please find the below mentioned link to check your Card Delivery Status.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, As per your profile you are not eligible for a Physical Card. The assigned Virtual Card you can use for all types of online transactions. '
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['7', '7. How will I pay my dues?', 'How will I pay my dues?'],
      content:
        '<h4>What is the status of my Card Delivery?</h4><br>' +
        '<p>Customers can pay their dues through their RedCarpet app or website. Note :- We never accept payment by cash.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Customers can pay their dues through their RedCarpet app or website. Note :- We never accept payment by cash.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['8', '8. How to Reload?', 'How to Reload?'],
      content:
        '<h4>How to Reload?</h4><br>' +
        '<h3>Dear User,</h3>' +
        '<p>You can follow the steps mentioned below to Reload your card.</p>' +
        '<p>Step 1 - Open RedCarpet mobile app.</p>' +
        '<p>Step 2 - Click on the smart card tab</p>' +
        '<p>Step 3 - Click on the Reload tab to place your request.</p>' +
        '<p>Step 4 - You will receive a notification as we approve your credit limit.</p>' +
        '<p>Step 5 - Pay accordingly using the pay fee button. According to the Approval limit, you can choose the amount for a reload and pay reload fee.</p>' +
        '<p>Step 6 -1 You will receive a confirmation notification on the RedCarpet app.</p>' +
        '<p><strong>Note :-</strong> In case you don’t have any approved limit then you are not eligible for Reload.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, As per your profile you are not eligible for a Physical Card. The assigned Virtual Card you can use for all types of online transactions. '
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['9', '9. Why did my Profile get rejected?', 'Why did my Profile get rejected?'],
      content:
        '<h4>Why did my Profile get rejected?</h4><br>' +
        '<p>Apologies for the inconvenience caused. In case your profile might have been rejected, that is because of our internal policies. We can not disclose the further information in detail. Hope you can understand.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Apologies for the inconvenience caused. In case your profile might have been rejected, that is because of our internal policies. We can not disclose the further information in detail. Hope you can understand.'
      ]
    },

    {
      question: ['We are happy to help you.'],
      command: ['1', '1. Existing User', 'Existing User'],
      content:
        '<h4>Please choose your options</h4><br>' +
        '<a class="btn">1. For Reload</a><br>' +
        '<a class="btn">2. Card Related Queries</a><br>' +
        '<a class="btn">3. Transaction Related Queries</a><br>' +
        '<a class="btn">4. Repayment Related Queries</a><br>' +
        '<a class="btn">5. Bill Related Queries</a><br>' +
        '<a class="btn">6. NOC or CIBIL Related Queries</a><br>' +
        '<a class="btn">7. Fraud or Recovery Agent Complaint</a><br>' +
        '<a class="btn">8. Refund Related Queries</a><br>' +
        '<a class="btn">9. To make changes in your Profile</a><br>' +
        '<a class="btn">#. For the Main Menu</a><br>' +
        '<a class="btn">*. To repeat the Menu</a>',
      voiceFile: '',
      prevoice: [
        'Press 1 for Reload',
        'Press 2 for Card Related Queries',
        'Press 3 for Transaction Related Queries',
        'Press 4 for Repayment Related Queries',
        'Press 5 for Bill Related Queries',
        'Press 6 for NOC or CIBIL Related Queries',
        'Press 7 for Fraud or Recovery Agent Complaint',
        'Press 8 for Refund Related Queries',
        'Press 9 for To make changes in your Profile',
        'Press # for the Main Menu',
        'Press * for To repeat the Menu'
      ],
      voice: [],
      voice_digits: {
        mapping: {
          '1': 'For Reload',
          '2': 'Card Related Queries',
          '3': 'Transaction Related Queries',
          '4': 'Repayment Related Queries',
          '5': 'Bill Related Queries',
          '6': 'NOC or CIBIL Related Queries',
          '7': 'Fraud or Recovery Agent Complaint',
          '8': 'Refund Related Queries',
          '9': 'To make changes in your Profile',
          '#': 'For the Main Menu',
          '*': 'To repeat the Menu'
        }
      }
    },

    {
      question: ['Please choose your options'],
      command: ['1', '1. For Reload', 'For Reload'],
      content:
        '<h4>For Reload</h4><br>' +
        '<a class="btn">1. Unable to find Reload Option in my app</a><br>' +
        '<a class="btn">2. Paid Reload Fees but Reload not done</a><br>' +
        '<a class="btn">3. Transaction Related Queries</a><br>' +
        '<a class="btn">*. For previous Menu</a><br>' +
        '<a class="btn">#. For Main Menu</a><br>' +
        '<a class="btn">0. To repeat the Menu</a><br>',
      voiceFile: '',
      prevoice: [
        'Press 1 for Unable to find Reload Option in my app',
        'Press 2 for Paid Reload Fees but Reload not done',
        'Press 3 for Transaction Related Queries',
        'Press * for Previous Menu',
        'Press # for Main Menu',
        'Press 0 for To repeat the Menu'
      ],
      voice: [],
      voice_digits: {
        mapping: {
          '1': 'Unable to find Reload Option in my App',
          '2': 'Paid Reload Fees but Reload not done',
          '3': 'Transaction Related Queries',
          '*': 'For Previous Menu',
          '#': 'For Main Menu',
          '0': 'To repeat the Menu'
        }
      }
    },
    {
      question: ['For Reload'],
      command: ['1', '1. Unable to find Reload Option in my App', 'Unable to find Reload Option in my App'],
      content:
        '<h4>Unable to find Reload Option in my App</h4><br>' +
        '<p>Dear User, We would request you to Reinstall or Update your RedCarpet app to the latest version. Even though facing the same issue please write us on <a href="mailto:support@redcapetup.com">Support@redcapetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We would request you to Reinstall or Update your RedCarpet app to the latest version. Even though facing the same issue please write us on <a href="mailto:support@redcapetup.com">Support@redcapetup.com</a>'
      ]
    },

    {
      question: ['For Reload'],
      command: ['2', '2. Paid Reload Fees but Reload not done', 'Paid Reload Fees but Reload not done'],
      content:
        '<h4>Paid Reload Fees but Reload not done</h4><br>' +
        '<p><strong>Type - 1.</strong> Dear User, Latest Reload Fees Paid by you is of INR _____ and the Reload has been completed against the paid amount. Kindly check your RedCarpet app properly.</p>' +
        '<p><strong>Type - 2.</strong> Dear User, Latest Reload fees paid by is of INR _____, and the Reload has not been completed against the paid amount. Would request you kindly wait for next 24-48 Hrs. to get the money disbursed in your Card.</p>' +
        '<p><strong>Type -3.</strong>  In case customer has not paid any amount against his Reload</p>' +
        '<p>Dear User, We have not received any payment against your Reload Fees. In case your payment is deducted please check with your concerned bank. For further queries write us on <a href="mailto:support@redcapetup.com">Support@redcapetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We would request you to Reinstall or Update your RedCarpet app to the latest version. Even though facing the same issue please write us on <a href="mailto:support@redcapetup.com">Support@redcapetup.com</a>'
      ]
    },

    {
      question: ['For Reload'],
      command: ['3', '3. How much do I have to pay to Reload my Card?', 'How much do I have to pay to Reload my Card?'],
      content:
        '<h4>How much do I have to pay to Reload my Card?</h4><br>' +
        '<p>Dear User, Please check your RedCarpet app to know the exact amount to Pay for Reload your Card. We charge roughly between 3 to 6 % for the Reload amount.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, Please check your RedCarpet app to know the exact amount to Pay for Reload your Card. We charge roughly between 3 to 6 % for the Reload amount.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['2', '2. Card Related Queries', 'Card Related Queries'],
      content:
        '<h4>Card Related Queries</h4><br>' +
        '<a class="btn">1. Card Delivery Issues</a><br>' +
        '<a class="btn">2. How to Activate my Ruby Card?</a><br>' +
        '<a class="btn">3. How to generate/change my Card PIN?</a><br>' +
        '<a class="btn">4. How to Block/Unblock my Card?</a><br>' +
        '<a class="btn">5. Unable to make transactions with my Card?</a><br>' +
        '<a class="btn">*. For previous Menu</a><br>' +
        '<a class="btn">#. For Main Menu</a><br>' +
        '<a class="btn">0. To repeat the information</a>',
      voiceFile: '',
      prevoice: [
        'Press 1 for Card Delivery Issues',
        'Press 2 for How to Activate my Ruby Card?',
        'Press 3 for How to generate/change my Card PIN?',
        'Press 4 for How to Block/Unblock my Card?',
        'Press 5 for Unable to make transactions with my Card?',
        'Press * for previous Menu',
        'Press # for Main Menu',
        'Press 0 for To repeat the information'
      ],
      voice: [],
      voice_digits: {
        mapping: {
          '1': 'Card Delivery Issues',
          '2': 'How to Activate my Ruby Card?',
          '3': 'How to generate/change my Card PIN?',
          '4': 'How to Block/Unblock my Card?',
          '5': 'Unable to make transactions with my Card?',
          '*': 'For previous Menu',
          '#': 'For Main Menu',
          '0': 'To repeat the information'
        }
      }
    },

    {
      question: ['Card Related Queries'],
      command: ['1', '1. Card Delivery Issues', 'Card Delivery Issues'],
      content:
        '<h4>Card Delivery Issues</h4><br>' +
        '<p>Dear User, Please track your RedCarpet Card with the below mentioned link.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We found, You have been assigned with a Virtual Card which can not be delivered to you. This card you can use to make all types of online transactions.'
      ]
    },

    {
      question: ['Card Related Queries'],
      command: ['2', '2. How to Activate my Ruby Card?', 'How to Activate my Ruby Card?'],
      content:
        '<h4>How to Activate my Ruby Card?</h4><br>' +
        '<p><strong>Dear User,</strong></p><br>' +
        '<p>Please find below mentioned steps to activate your Virtual Card.</p><br>' +
        '<p>Step 1 - Log in to the RedCarpet App.</p><br>' +
        '<p>Step 2 - Click on your card.</p><br>' +
        '<p>Step 3 - Click on the Activate Card button.</p><br>' +
        '<p>Step 4 - Fill in your details.</p><br>' +
        '<p>Step 5 - Tap on “NO” before you proceed further.</p><br>' +
        '<p>Step 6 - Enter the OTP received on the registered number.Your card will be activated.</p><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We found, You have been assigned with a Virtual Card which can not be delivered to you. This card you can use to make all types of online transactions.'
      ]
    },
    {
      question: ['Card Related Queries'],
      command: ['3', '3. How to generate/change my Card PIN?', 'How to generate/change my Card PIN?'],
      content:
        '<h4>How to generate/change my Card PIN?</h4><br>' +
        '<p>Please follow the below mentioned steps to Reset your Ruby Card PIN.</p><br>' +
        '<p>Step 1 - Open your Redcarpet app</p><br>' +
        '<p>Step 2 - Tap on Reset Pin.</p><br>' +
        '<p>Step 3 - Enter your details.(DOB)</p><br>' +
        '<p>Step 4 - Choose a Pin of your Choice.</p><br>' +
        '<p>Step 5 - You will receive an OTP. Enter the OTP correctly.</p><br>' +
        '<p>Step 6 - You will receive a message of a successful Pin Reset.</p><br>' +
        '<p>Kindly look at the video below for further details. <a href="https://onedirect-production-attachments.storage.googleapis.com/chatbot/3049/attachments/prod/2020/07/25/1595662776_chatbot-1981-attachments-prod-2020-03-03-1583244044-how-to-change_bE7crTvu.compressed.mp4" target="_blank">Click Here</a></p>'
      ,
      voiceFile: '',
      prevoice: [],
      voice: []
    },
    {
      question: ['Card Related Queries'],
      command: ['4', '4. How to Block/Unblock my Card?', 'How to Block/Unblock my Card?'],
      content:
        '<h4>How to Block/Unblock my Card?</h4><br>' +
        '<a class="btn">1. Block your Card?</a><br>' +
        '<a class="btn">2. Unblock your Card?</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for Block your Card',
        'Press 2 for Unblock your Card'
      ]
    },
    {
      question: ['How to Block/Unblock my Card?'],
      command: ['1', '1. Block your Card?', 'Block your Card?'],
      content:
        '<h4>Block your Card?</h4><br>' +
        '<p>Please follow the below mentioned steps to Unlock your Card.</p><br>' +
        '<p>Step 1 - Open your RedCarpet App.</p><br>' +
        '<p>Step 2 - Go to the Profile Section.</p><br>' +
        '<p>Step 3 - Tap on "Card Block"</p><br>' +
        '<p>Step 4 - Tap on Yes.</p><br>' +
        '<p>Step 5 - You will receive an OTP. Enter it correctly.</p><br>' +
        '<p>Step 6 - Your card would be Blocked successfully.</p><br>',
      voiceFile: '',
      prevoice: [],
      voice: []
    },
    {
      question: ['How to Block/Unblock my Card?'],
      command: ['2', '2. Unblock your Card?', 'Unblock your Card?'],
      content:
        '<h4>Unblock your Card?</h4><br>' +
        '<p><strong>Dear User,</strong> In case you have blocked your card, It could not be unblocked because of the internal policies. Now you have to apply for a New Card to use the RedCarpet services.</p><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, In case you have blocked your card, It could not be unblocked because of the internal policies. Now you have to apply for a New Card to use the RedCarpet services.'
      ]
    },
    {
      question: ['Card Related Queries'],
      command: ['5', '5. Unable to make transactions with my Card?', 'Unable to make transactions with my Card?'],
      content:
        '<h4>Unable to make transactions with my Card?</h4><br>' +
        '<p>Dear User, You can use your card to make all types of online and offline transactions. It is not possible to transfer the amount from a RedCarpet Card to your Bank. It could be possible only by using our card on online e-Wallets like; Paytm, Mobikwik, etc.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, You can use your card to make all types of online and offline transactions. It is not possible to transfer the amount from a RedCarpet Card to your Bank. It could be possible only by using our card on online e-Wallets like; Paytm, Mobikwik, etc.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['3', '3. Transaction Related Queries', 'Transaction Related Queries'],
      content:
        '<h4>My Online Transaction got Failed</h4><br>' +
        '<a class="btn">1. My Online Transaction got Failed</a><br>' +
        '<a class="btn">2. My ATM or POS Transaction got Failed</a>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for My Online Transaction got Failed',
        'Press 2 for My ATM or POS Transaction got Failed'
      ]
    },
    {
      question: ['Transaction Related Queries'],
      command: ['1', '1. My Online Transaction got Failed', 'My Online Transaction got Failed'],
      content:
        '<h4>My Online Transaction got Failed</h4><br>' +
        '<p>If possible Customer’s Contact details should be shared to the Team on <a href="mailto:Support@RedCarpetup.com">Support@RedCarpetup.com</a></p><br>' +
        '<p><strong>Dear User,</strong>Apologies for the inconvenience caused to you. We experienced the highlighted issue because of some technical glitch at partner’s end. For this type of transaction failures cases, We’ll refund your amount within 7-10 working days. For further queries you can write us on Support@redcarpetup.com with required details i.e; User Id, relevant screenshots.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Apologies for the inconvenience caused to you. For transaction failure cases, We’ll refund your amount within 7-10 working days. For further queries you can write to us on Support@redcarpetup.com with required details i.e; User Id, relevant screenshots.'
      ]
    },
    {
      question: ['Transaction Related Queries'],
      command: ['2', '2. My ATM or POS Transaction got Failed', 'My ATM or POS Transaction got Failed'],
      content:
        '<h4>Transaction Related Queries</h4><br>' +
        '<p>If possible Customer’s Contact details should be shared to the Team on <a href="mailto:Support@RedCarpetup.com">Support@RedCarpetup.com</a></p><br>' +
        '<p><strong>Dear User,</strong>Apologies for the inconvenience caused to you. For transaction failure cases, We’ll refund your amount within 7-10 working days. For further queries you can write to us on Support@redcarpetup.com with required details i.e; User Id, relevant screenshots.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Apologies for the inconvenience caused to you. For transaction failure cases, We’ll refund your amount within 7-10 working days. For further queries you can write to us on Support@redcarpetup.com with required details i.e; User Id, relevant screenshots.'
      ]
    },

    {
      question: ['Please choose your options'],
      command: ['4', '4. Repayment Related Queries', 'Repayment Related Queries'],
      content:
        '<h4>Repayment Related Queries</h4><br>' +
        '<a class="btn">1. Unable to find the exact Min and Max Amount to pay against my dues?</a><br>' +
        '<a class="btn">2. How can I pay my dues?</a><br>' +
        '<a class="btn">3. I paid my dues but the payment was not updated?</a><br>' +
        '<a class="btn">4. Unable to pay neither from your RedCarpet app nor Website</a><br>' +
        '<a class="btn">5. Can I pay my dues later?</a><br>' +
        '<a class="btn">6. I don’t have any dues but getting calls or SMS from RedCarpet?</a><br>' +
        '<a class="btn">7. I cleared my dues but your app still asking for the payment</a>',
      voiceFile: '',
      prevoice: [
        'Press 1 for Unable to find the exact Min and Max Amount to pay against my dues?',
        'Press 2 for How can I pay my dues?',
        'Press 3 for I paid my dues but the payment was not updated?',
        'Press 4 for Unable to pay neither from your RedCarpet app nor Website',
        'Press 5 for Can I pay my dues later?',
        'Press 6 for I don’t have any dues but getting calls or SMS from RedCarpet?',
        'Press 7 for I cleared my dues but your app still asking for the payment',
      ],
      voice: []
    },
    {
      question: ['Repayment Related Queries'],
      command: ['1', '1. Unable to find the exact Min and Max Amount to pay against my dues?', 'Unable to find the exact Min and Max Amount to pay against my dues?'],
      content:
        '<h4>Unable to find the exact Min and Max Amount to pay against my dues?</h4><br>' +
        '<p><strong>Dear User,</strong>You have a Minimum Amount of INR ________ and Maximum amount of INR _____ which you have to pay before 15th of this month.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You have a Minimum Amount of INR ________ and Maximum amount of INR _____ which you have to pay before 15th of this month.'
      ]
    },
    {
      question: ['Repayment Related Queries'],
      command: ['2', '2. How can I pay my dues?', 'How can I pay my dues?'],
      content:
        '<h4>How can I pay my dues?</h4><br>' +
        '<p><strong>Dear User,</strong>You can pay your dues with your Debit Card or Netbanking on our RedCarpet App or Website. In case you face some problem please write us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You can pay your dues with your Debit Card or Netbanking on our RedCarpet App or Website. In case you face some problem please write us on Support@redcarpetup.com'
      ]
    },
    {
      question: ['Repayment Related Queries'],
      command: ['3', '3. I paid my dues but the payment was not updated?', 'I paid my dues but the payment was not updated?'],
      content:
        '<h4>How can I pay my dues?</h4><br>' +
        '<p><strong>Dear User,</strong>We have received the last payment from your end of INR ____ on (Date). In case you are unable to find the latest payment status, We would request you to contact your bank. For further queries please write us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'We have received the last payment from your end of INR ____ on (Date). In case you are unable to find the latest payment status, We would request you to contact your bank. For further queries please write us on Support@redcarpetup.com'
      ]
    },
    {
      question: ['Repayment Related Queries'],
      command: ['4', '4. Unable to pay neither from your RedCarpet app nor Website', 'Unable to pay neither from your RedCarpet app nor Website'],
      content:
        '<h4>Unable to pay neither from your RedCarpet app nor Website</h4><br>' +
        '<p><strong>Dear User,</strong>Kindly reinstall or update your Redcarpet app to make the payment. Even though you are unable to make the payment, Please write to us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a> will share the required link.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Kindly reinstall or update your Redcarpet app to make the payment. Even though you are unable to make the payment, Please write to us on Support@redcarpetup.com will share the required link.'
      ]
    },

    {
      question: ['Repayment Related Queries'],
      command: ['5', '5. Can I pay my dues later?', 'Can I pay my dues later?'],
      content:
        '<h4>Can I pay my dues later?</h4><br>' +
        '<p><strong>Dear User,</strong>As per our current policies we do not have any option to provide some extra time to pay against your dues. We would request you, Kindly pay your dues on time to avoid interest or late fine charges</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'As per our current policies we do not have any option to provide some extra time to pay against your dues. We would request you, Kindly pay your dues on time to avoid interest or late fine charges.'
      ]
    },

    {
      question: ['Repayment Related Queries'],
      command: ['6', '6. I don’t have any dues but getting calls or SMS from RedCarpet?', 'I don’t have any dues but getting calls or SMS from RedCarpet?'],
      content:
        '<h4>I don’t have any dues but getting calls or SMS from RedCarpet?</h4><br>' +
        '<p><strong>Dear User,</strong>As per your profile we found there is a due amount of INR ______ which you still have to pay against your dues. That is why you might be getting calls or SMS from our side.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'As per your profile we found there is a due amount of INR ______ which you still have to pay against your dues. That is why you might be getting calls or SMS from our side.'
      ]
    },

    {
      question: ['Repayment Related Queries'],
      command: ['7', '7. I cleared my dues but your app still asking for the payment', 'I cleared my dues but your app still asking for the payment'],
      content:
        '<h4>I cleared my dues but your app still asking for the payment</h4><br>' +
        '<p><strong>Dear User,</strong>As per your profile we found there is a due amount of INR ______ which you still have to pay against your dues. That is why you might be getting dues in your RedCarpet app.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'As per your profile we found there is a due amount of INR ______ which you still have to pay against your dues. That is why you might be getting dues in your RedCarpet app.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['5', '5. Bill Related Queries', 'Bill Related Queries'],
      content:
        '<h4>Bill Related Queries</h4><br>' +
        '<a class="btn">1. To know when Bill gets generated?</a><br>' +
        '<a class="btn">2. To know why the Bill is not generated yet?</a><br>' +
        '<a class="btn">3. To know how to pay dues in EMIs?</a><br>' +
        '<a class="btn">4. To know the interest charged on your dues?</a><br>' +
        '<a class="btn">5. To know why a late fine has been charged?</a><br>' +
        '<a class="btn">6. To know difference between Min and Max amount</a><br>' +
        '<a class="btn">7. To know Card Statement</a><br>' +
        '<a class="btn">8. What would be the last date of payment?</a>',
      voiceFile: '',
      prevoice: [
        'Press 1 for To know when Bill gets generated?',
        'Press 2 for To know why the Bill is not generated yet?',
        'Press 3 for To know how to pay dues in EMIs?',
        'Press 4 for To know the interest charged on your dues?',
        'Press 5 for To know why a late fine has been charged?',
        'Press 6 for To know difference between Min and Max amount',
        'Press 7 for To know Card Statement',
        'Press 8 for What would be the last date of payment?',
      ],
      voice: []
    },
    {
      question: ['Bill Related Queries'],
      command: ['1', '1. To know when Bill gets generated?', 'To know when Bill gets generated?'],
      content:
        '<h4>To know when Bill gets generated?</h4><br>' +
        '<p><strong>Dear User,</strong>Your bill gets generated till 3rd of every month against your all spending’s in the last month.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Your bill gets generated till 3rd of every month against your all spending’s in the last month.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['2', '2. To know why the Bill is not generated yet?', 'To know why the Bill is not generated yet?'],
      content:
        '<h4>To know why the Bill is not generated yet?</h4><br>' +
        '<p><strong>Dear User,</strong>Your bill will be generated soon. The same will be informed to you soon on your registered mobile number through SMS.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Your bill will be generated soon. The same will be informed to you soon on your registered mobile number through SMS.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['3', '3. To know how to pay dues in EMIs?', 'To know how to pay dues in EMIs?'],
      content:
        '<h4>To know how to pay dues in EMIs?</h4><br>' +
        '<p><strong>Dear User,</strong>You can simply convert your dues into EMIs by paying your minimum amount against your dues.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You can simply convert your dues into EMIs by paying your minimum amount against your dues.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['4', '4. To know the interest charged on your dues?', 'To know the interest charged on your dues?'],
      content:
        '<h4To know the interest charged on your dues?</h4><br>' +
        '<p><strong>Dear User,</strong>You have been charged for the interest amount of INR ______ because you have paid minimum or less than the maximum amount against your dues.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You have been charged for the interest amount of INR ______ because you have paid minimum or less than the maximum amount against your dues.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['5', '5. To know why a late fine has been charged?', 'To know why a late fine has been charged?'],
      content:
        '<h4>To know why a late fine has been charged?</h4><br>' +
        '<p><strong>Dear User,</strong>You might have paid your dues after the 15th of the month which is the due date on which you should have to pay your dues.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You might have paid your dues after the 15th of the month which is the due date on which you should have to pay your dues.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['6', '6. To know difference between Min and Max amount', 'To know difference between Min and Max amount'],
      content:
        '<h4>To know difference between Min and Max amount</h4><br>' +
        '<p><strong>Dear User,</strong>Your monthly EMI is considered as a Minimum Amount which you have to pay before 15th of every month. And the total outstanding amount considered as a Maximum Amount.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Your monthly EMI is considered as a Minimum Amount which you have to pay before 15th of every month. And the total outstanding amount considered as a Maximum Amount.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['7', '7. To know Card Statement', 'To know Card Statement'],
      content:
        '<h4>To know Card Statement</h4><br>' +
        '<p><strong>Dear User,</strong>You can check your Card Statement from your RedCarpet app by clicking on the profile option.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'You can check your Card Statement from your RedCarpet app by clicking on the profile option.'
      ]
    },
    {
      question: ['Bill Related Queries'],
      command: ['8', '8. What would be the last date of payment?', 'What would be the last date of payment?'],
      content:
        '<h4>What would be the last date of payment?</h4><br>' +
        '<p><strong>Dear User,</strong>The last payment date is the 15th of every month.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'The last payment date is the 15th of every month.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['6', '6. NOC or CIBIL Related Queries', 'NOC or CIBIL Related Queries'],
      content:
        '<h4>NOC or CIBIL Related Queries</h4><br>' +
        '<a class="btn">1. For NOC</a><br>' +
        '<a class="btn">2. For CIBIL Update</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 For NOC',
        'Press 2 For CIBIL Update',
      ]
    },
    {
      question: ['NOC or CIBIL Related Queries'],
      command: ['1', '1. For NOC', 'For NOC'],
      content:
        '<h4>NOC</h4><br>' +
        '<p><strong>Dear User,</strong>Hopefully you have paid your dues against your acquired loan against which you are requesting for an NOC because NOC can be issued only, Once the dues paid by you.</p>' +
        '<a class="btn">1. In case paid your dues </a><br>' +
        '<a class="btn">2. In case not paid your dues</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'Hopefully you have paid your dues against your acquired loan against which you are requesting for an NOC because NOC can be issued only, Once the dues paid by you.',
        'Press 1 for In case paid your dues',
        'Press 2 for In case not paid your dues'
      ]
    },
    {
      question: ['NOC'],
      command: ['1', '1. In case paid your dues', 'In case paid your dues'],
      content:
        '<h4>In case paid your dues</h4><br>' +
        '<p>If possible the details of the user should be shared to <a href="mailto: quality@redcarpetup.com"> quality@redcarpetup.com</a>or Data can be downloaded by the concerned team the next day to work upon it.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'If you already paid your dues and want to get NOC against your acquired loan. It will be shared by our team to you within 14 working days on your email address after verifying the data. In case they have some discrepancy, They will inform you accordingly.'
      ]
    },
    {
      question: ['NOC'],
      command: ['2', '2. In case not paid your dues', 'In case not paid your dues'],
      content:
        '<h4>In case not paid your dues</h4><br>' +
        '<p><strong>Dear User,</strong>As already mentioned we can issue the NOC only when the dues get cleared by the user so we would request you kindly pay your dues first to get an NOC.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'As already mentioned we can issue the NOC only when the dues get cleared by the user so we would request you kindly pay your dues first to get an NOC.'
      ]
    },
    {
      question: ['NOC or CIBIL Related Queries'],
      command: ['2', '2. CIBIL Update', 'CIBIL Update'],
      content:
        '<h4>CIBIL</h4><br>' +
        '<p><strong>Dear User,</strong>In case you want to update your CIBIL report we would request you to share your latest CIBIL report on <a href="mailto:quality@redcarpetup.com">quality@redcarpetup.com</a> In case you don’t have any dues pending against your loan, You CIBIL will be updated within 45 working days</p>' +
        '<a class="btn">1. In case paid your dues </a><br>' +
        '<a class="btn">2. In case not paid your dues</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User,',
        'In case you want to update your CIBIL report we would request you to share your latest CIBIL report on quality@redcarpetup.com. In case you don’t have any dues pending against your loan, You CIBIL will be updated within 45 working days.'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['7', '7. Fraud or Recovery Agent Complaint', 'Fraud or Recovery Agent Complaint'],
      content:
        '<h4>Fraud or Recovery Agent Complaint</h4><br>' +
        '<a class="btn">1. To complaint against Recovery Agent</a><br>' +
        '<a class="btn">2. To inform Fraud happened with your Card</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for To complaint against Recovery Agent',
        'Press 2 for To inform Fraud happened with your Card',
      ]
    },
    {
      question: ['Fraud or Recovery Agent Complaint'],
      command: ['1', '1. To complaint against Recovery Agent', 'To complaint against Recovery Agent'],
      content:
        '<h4>To complaint against Recovery Agent</h4><br>' +
        '<p><strong>Dear User,</strong> In case anytype of misbehaviour is made by our Recovery Agents. We will take the best possible action against the agent and notify you on this soon. Till we would request you to pay your dues on time to avoid collection calls.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, In case anytype of misbehaviour is made by our Recovery Agents. We will take the best possible action against the agent and notify you on this soon. Till we would request you to pay your dues on time to avoid collection calls.',
      ]
    },
    {
      question: ['Fraud or Recovery Agent Complaint'],
      command: ['2', '2. To inform Fraud happened with your Card', 'To inform Fraud happened with your Card'],
      content:
        '<h4>To inform Fraud happened with your Card</h4><br>' +
        '<p><strong>Dear User,</strong> In case any type of fraud had happened with your RedCarpet Card, Please file a complaint to the nearest police station against the accused. In case any help from our side is required please feel free to write us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a>.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, In case any type of fraud had happened with your RedCarpet Card, Please file a complaint to the nearest police station against the accused. In case any help from our side is required please feel free to write us on Support@redcarpetup.com.',
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['8', '8. Refund Related Queries', 'Refund Related Queries'],
      content:
        '<h4>Refund Related Queries</h4><br>' +
        '<a class="btn">1. To request Refund</a><br>' +
        '<a class="btn">2. To know status of your Refund</a><br>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for To request Refund',
        'Press 2 for To know status of your Refund',
      ]
    },
    {
      question: ['Refund Related Queries'],
      command: ['1', '1. To request Refund', 'To request Refund'],
      content:
        '<h4>To request Refund</h4><br>' +
        '<a class="btn">1. To get refund against the recent transaction</a>' +
        '<a class="btn">2. To get refund against recent paid Reload Fees</a>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for To get refund against the recent transaction',
        'Press 2 for To get refund against recent paid Reload Fees'
      ]
    },

    {
      question: ['To request Refund'],
      command: ['1', '1. To get refund against the recent transaction', 'To get refund against the recent transaction'],
      content:
        '<h4>To get refund against the recent transaction</h4><br>' +
        '<p><strong>Dear User,</strong> Recent transactions made by you are completed and received by the merchant. In case there is some error happened, We would request you kindly share the required screenshots on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, Recent transactions made by you are completed and received by the merchant. In case there is some error happened, We would request you kindly share the required screenshots on Support@redcarpetup.com'
      ]
    },
    {
      question: ['To request Refund'],
      command: ['2', '2. To get refund against recent paid Reload Fees', 'To get refund against recent paid Reload Fees'],
      content:
        '<h4>To get refund against recent paid Reload Fees</h4><br>' +
        '<p><strong>Dear User,</strong> We regret to inform you that we have not received any payment against Reload fees from your end. Would request you to please contact your bank to check payment status. In case further queries please write us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, We regret to inform you that we have not received any payment against Reload fees from your end. Would request you to please contact your bank to check payment status. In case further queries please write us on Support@redcarpetup.com'
      ]
    },

    {
      question: ['Refund Related Queries'],
      command: ['2', '2. To know status of your Refund', 'To know status of your Refund'],
      content:
        '<h4>To know status of your Refund</h4><br>' +
        '<p><strong>Dear User,</strong>To get the status of your refund please write us on <a href="mailto:Support@redcarpetup.com">Support@redcarpetup.com</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, To get the status of your refund please write us on Support@redcarpetup.com'
      ]
    },
    {
      question: ['Please choose your options'],
      command: ['9', '9. To make changes in your Profile', 'To make changes in your Profile'],
      content:
        '<h4>To make changes in your Profile</h4><br>' +
        '<a class="btn">1. To know how to change your registered mobile number</a><br>' +
        '<a class="btn">2. To know how to change your registered address</a><br>' +
        '<a class="btn">3. To know how to change upload required documents</a>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Press 1 for To know how to change your registered mobile number',
        'Press 2 for To know how to change your registered address',
        'Press 3 for To know how to change upload required documents'
      ]
    },
    {
      question: ['To make changes in your Profile'],
      command: ['1', '1. To know how to change your registered mobile number', 'To know how to change your registered mobile number'],
      content:
        '<h4>To know how to change your registered mobile number</h4><br>' +
        '<p><strong>Dear User,</strong>We would like to request you kindly fill up the attached form with the help of the given link to get us more information about your Phone Number update request. <a href="https://forms.gle/2LCT6RbAYszwBzYi7" target="_blank">Click Here</a></p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'As a proof to get your Phone Number updated we require an FIR copy of your phone number lost or an email confirmation shared by your service provider that your requested number no longer belongs to you. We have forwarded your request to our concerned team. It will be done within 2-3 working days.'
      ]
    },
    {
      question: ['To make changes in your Profile'],
      command: ['2', '2. To know how to change your registered address', 'To know how to change your registered address'],
      content:
        '<h4>To know how to change your registered address</h4><br>' +
        '<p><strong>Dear User,</strong>Our current policies do not allow us to re-update your Address on the RedCarpet app</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, Our current policies do not allow us to re-update your Address on the RedCarpet app.'
      ]
    },
    {
      question: ['To make changes in your Profile'],
      command: ['3', '3. To know how to change upload required documents', 'To know how to change upload required documents'],
      content:
        '<h4>To know how to change upload required documents</h4><br>' +
        '<p><strong>Dear User,</strong>Our current policies do not allow us to re-upload your documents on the RedCarpet app.</p>',
      voiceFile: '',
      prevoice: [],
      voice: [
        'Dear User, Our current policies do not allow us to re-upload your documents on the RedCarpet app.'
      ]
    },

    // twice close bot
    {
      question: ['thankyou'],
      command: ['end'],
      content:
        '<h4>Thanking you for calling RedCarpet</h4>',
      prevoice: '',
      voice: [
        'Thanking you for calling RedCarpet'
      ]
    },

  ];

  @ViewChild('scrollBottom') private scrollBottom: ElementRef;

  constructor(private elementRef: ElementRef, private ngZone: NgZone, private fb: FormBuilder) {
    this.registerForm = fb.group({
      lastname: ['', Validators.required]
    })
  }

  ngOnInit() {
    // this.muteVoice();

    const botMessage = new Message('bot', this.getBotMessage('Welcome'));
    setTimeout(() => {
      this.conversation.next([botMessage]);
      this.scrollToBottom();
    }, 1000);

    this.conversation.subscribe((val) => {
      this.messages = this.messages.concat(val);

      setTimeout(() => {
        this.scrollToBottom();
      }, 1500);
    });
  }

  ngAfterViewChecked() {
    const elements = document.getElementsByClassName('btn');
    const formElements = document.getElementsByClassName('form');

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener(
        'click',
        this.sendMessagebtnQueryGeneric.bind(this),
        false
      );
    }
    if (this.elementRef.nativeElement.querySelector('#form')) {
      this.elementRef.nativeElement
        .querySelector('#form')
        .addEventListener('submit', this.formValidation.bind(this));
    }
    if (this.elementRef.nativeElement.querySelector('#form-1')) {
      this.elementRef.nativeElement
        .querySelector('#form-1')
        .addEventListener('submit', this.formValidation.bind(this));
    }


  }

  formValidation(e: any) {
    e.preventDefault();
    this.sendMessagebtnQuery(e.target.getAttribute("rel"));
  }

  async validateInputField(inputText: any) {
    if (inputText.value === '') {
      console.log('Input field is required');
      return false
    } else {
      return true
    }
  }

  enterHashKey(value: string) {
    console.log(value);

  }

  handleKeyboardEvent(evt: KeyboardEvent) {
    var inputEl = document.getElementById('mobile_no');

    let now = Date.now();

    if (now - this.lastClick < 1000) {
      console.log("double")
      this.currentQuestion = 'thankyou';
      this.sendMessagebtnQuery('end');
      return;
    } else {
      this.sendMessagebtnQuery(evt.key);
    }

    this.lastClick = now;

    // inputEl?.addEventListener("keypress", function(el){
    //   if (evt.key === '') {
    //   }
    // })
  }


  sendMessagebtnQuery(query: any) {
    if (!this.botCallFlag) {
      speechSynthesis.cancel();
      this.userEnterdText = query;
      this.sendMessage();
      this.botCallFlag = true;
    }

    setTimeout(() => {
      this.botCallFlag = false;
    }, 500);
  }

  sendMessagebtnQueryGeneric(query: any) {
    if (!this.botCallFlag) {
      if (query.target.rel === 'Hindi') {
        this.botName = 'मोती'
      } else if (query.target.rel === 'Kannada') {
        this.botName = 'ಮೋತಿ';
      }
      speechSynthesis.cancel();
      this.userEnterdText = query.target.textContent;
      this.sendMessage();
      this.botCallFlag = true;

    }

    setTimeout(() => {
      this.botCallFlag = false;
    }, 500);
  }

  getBotAnswer(msg: string) {
    const userMessage = new Message('user', msg);
    this.conversation.next([userMessage]);
    const botMessage = new Message('bot', this.getBotMessage(msg));

    setTimeout(() => {
      this.conversation.next([botMessage]);
      this.scrollToBottom();
    }, 1000);
  }

  sendMessageEnter(e: any) {
    e.preventDefault();
    this.sendMessage();
  }

  sendMessage() {
    if (this.userEnterdText.trim().length > 0) {
      this.audio.pause();
      this.audio.currentTime = 0;
      this.getBotAnswer(this.userEnterdText);
    }
    this.userEnterdText = '';
  }

  getBotMessage(query: string) {
    console.log(this.currentQuestion);
    console.log(query);
    let answerEle: any = this.messageMap.filter((ele: any) => {
      let s = ele.question.find(
        (ele1: any) => ele1.toLowerCase() === this.currentQuestion.toLowerCase()
      );
      if (s !== undefined) {
        return ele;
      }
    });

    let answer = answerEle.filter((d: any) => {
      let s = d.command.find(
        (ele: any) => ele.toLowerCase() === query.toLowerCase()
      );
      if (s !== undefined) {
        return d;
      }
    });

    // console.log(answerEle);
    // console.log(answer);

    let data;

    if (answer.length > 0) {
      answer.map((data: any) => {
        let prevoice = data.prevoice;

        if (data.voiceFile) {
          this.playAudio(data.voiceFile);
        }

        if (prevoice.length > 0) {
          prevoice.map((response: any) => {
            if (response.trim().length > 0) {
              setTimeout(() => {
                this.speakInitiate(response);
              }, 0);
              // const userMessage = new Message('bot', response);
              // this.conversation.next([userMessage]);
            }
          });
          this.botCallFlag = true;
        }

        setTimeout(() => {
          this.speakInitiate(data.voice);
        }, 1000);
      });

      data = answer.map((d: any) => d.content);

      let str = data.toString();

      if (str.includes('<h4>')) {
        this.currentQuestion = str.substring(
          str.indexOf('<h4>') + 4,
          str.lastIndexOf('</h4>')
        );
      }
    } else {
      data = '<h5>Please enter valid command</h5>';
      this.speakInitiate('Please Enter valid command');
    }

    speechSynthesis.cancel();
    return data;
  }


  scrollToBottom(): void {
    try {
      this.elem = document.getElementById('chat_body');
      this.elem.scrollTop = this.elem.scrollHeight;
    } catch (err) {
    }
  }

  // ==========  Speaking ==============

  playAudio(file: string) {
    this.audio.src = '../../../motiHelpAssist/assets/voice/' + file + '.mp3';
    this.audio.load();
    this.audio.play().then();
  }


  initializeVoiceRecognitionCallback(): void {
    annyang.addCallback('error', (err: any) => {
      if (err.error === 'network') {
        this.voiceText = 'Internet is require';
        annyang.abort();
        this.ngZone.run(() => (this.voiceActiveSectionSuccess = true));
      } else if (this.voiceText === undefined) {
        this.ngZone.run(() => (this.voiceActiveSectionError = true));
        annyang.abort();
      }
    });

    annyang.addCallback('soundstart', (res: any) => {
      this.ngZone.run(() => (this.voiceActiveSectionListening = true));
    });

    annyang.addCallback('end', () => {
      if (this.voiceText === undefined) {
        this.ngZone.run(() => (this.voiceActiveSectionError = true));
        annyang.abort();
      }
    });

    annyang.addCallback('result', (userSaid: any) => {
      this.ngZone.run(() => (this.voiceActiveSectionError = false));

      let queryText: any = userSaid[0];

      annyang.abort();

      this.userEnterdText = this.voiceText = queryText;
      // this.sendMessage();

      this.ngZone.run(() => (this.voiceActiveSectionListening = false));
      this.ngZone.run(() => (this.voiceActiveSectionSuccess = true));
      this.ngZone.run(() => (this.voiceActiveSectionDisabled = true));
    });
  }

  startVoiceRecognition(): void {
    this.voiceActiveSectionDisabled = false;
    this.voiceActiveSectionError = false;
    this.voiceActiveSectionSuccess = false;
    this.voiceText = undefined;

    if (annyang) {
      let commands = {
        'start Voice': () => {
        },
      };

      annyang.addCommands(commands);

      this.initializeVoiceRecognitionCallback();

      annyang.start({autoRestart: false});
    }
  }

  closeVoiceRecognition(): void {
    this.voiceActiveSectionDisabled = true;
    this.voiceActiveSectionError = false;
    this.voiceActiveSectionSuccess = false;
    this.voiceActiveSectionListening = false;
    this.voiceText = undefined;

    if (annyang) {
      annyang.abort();
    }
  }

  speakInitiate(voice: any) {
    let voices = this.synth.getVoices()
    let botVoice = voices.findIndex(items => items.name === 'Google UK English Female');
    let botDefaultVoice = voices.findIndex(items => items.name === 'Microsoft Heera - English (India)');

    var utterThis = new SpeechSynthesisUtterance(voice);
    utterThis.volume = this.voiceVolume;
    // utterThis.rate = 1.5;
    utterThis.voice = botVoice > -1 ? voices[botVoice] : voices[botDefaultVoice];
    this.synth.speak(utterThis);
  }

  muteVoice() {
    this.voiceMuted = !this.voiceMuted;

    if (this.voiceMuted) {
      this.voiceVolume = 0;
      this.audio.volume = 0;
      speechSynthesis.cancel();
    } else {
      this.voiceVolume = 1;
      this.audio.volume = 1;
    }
  }

  ngOnDestroy() {
    speechSynthesis.cancel();
  }

  // private synthesizeSpeechFromText(
  // 	voice: SpeechSynthesisVoice,
  // 	rate: number,
  // 	text: string
  // 	) : void {

  // 	var utterance = new SpeechSynthesisUtterance( text );
  // 	utterance.voice = this.selectedVoice;
  // 	utterance.rate = rate;

  // 	speechSynthesis.speak( utterance );

  // }
}

