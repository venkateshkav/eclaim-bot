import { Injectable } from '@angular/core';

export class Message {
  constructor(public author: string, public content: any) {}
}

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  constructor() {}

}
